class LogIndexSerializer < ActiveModel::Serializer
  attributes :id , :activity_code , :status_changed_from , :status_changed_to ,:done_by , :container_id,#:activity_type 
  def activity_type
    object.activity.activity_type 
  end
  def activity_code 
    object.activity.activity_code
  end 

end
