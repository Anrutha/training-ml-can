class ContainerFilterSerializer < ActiveModel::Serializer
    attributes :container_id,:container_num , :yard ,:customer ,:owner_name, :activity , :activity_id, :activity_date , :status
  
    def container_num
      puts "############### serializer"
      object.container.container_num
    end
    def yard
      #Yard.find(Container.find(object.container_id).yard_name_id).yard_name
      object.container.yard.yard_name
    end
    def activity_id
        object.activity_code
    end
    def activity_date
      object.date
    end
    def customer
      #Customer.find(Container.find(object.container_id).customer_id).customer_name
      object.container.customer.customer_name
    end
    def owner_name
      object.container.container_owner_name
    end
    def activity
      object.activity_type
    end
    
  
  
  end
  