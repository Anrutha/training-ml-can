class ContainerShowSerializer < ActiveModel::Serializer
  attributes :id ,:yard_name,:customer, :container_owner_name , :submitter_initials ,:container_length , :container_height  , :container_type , :container_manufacture_year ,:container_num
  def yard_name
    # puts "****** serializer #{object.yard_name_id} yard name : #{Yard.find(object.yard_name_id).as_json}"
    # puts "*********** #{object.yard.yard_name}"
    object.yard.yard_name
  end
  def customer
    #Customer.find(object.customer_id).customer_name
    object.customer.customer_name
  end

  def container_height
    #ContainerHeight.find(object.container_height_id).measurement
    object.container_height.measurement

  end
  def container_length
    #ContainerLength.find(object.container_length_id).measurement
    object.container_length.measurement
  end
  def container_type
    #ContainerType.find(object.container_type_id).type_name
    object.container_type.type_name
  end




end
