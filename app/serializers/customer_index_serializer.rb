class CustomerIndexSerializer < ActiveModel::Serializer
  attributes :id , :customer_name , :owner_name , :email , :hourly_rate , :status
end
