class ContainerAllSerializer < ActiveModel::Serializer
  attributes :container_id,:container_num , :yard ,:customer ,:container_owner_name, :current_activity , :date , :status

    def container_num
    Container.find(object.container_id).container_num
  end
  def yard
    Yard.find(Container.find(object.container_id).yard_name_id).yard_name
  end
  def customer
    Customer.find(Container.find(object.container_id).customer_id).customer_name
  end
  def container_owner_name
    Container.find(object.container_id).container_owner_name
  end
  def current_activity
    object.activity_type
  end
  


end
