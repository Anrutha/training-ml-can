class StatusType < ApplicationRecord
    scope :quote, -> { where(type_name: ['quote_draft', 'quote_pending', 'quote_issued','quote_approved']) }
    scope :repair, -> { where(type_name:['repair_draft','repair_done'])}
    scope :inspection,->{where(type_name:['inspection_done','inspection_draft'])}
end
