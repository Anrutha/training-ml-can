class RepairListDetail < ApplicationRecord
  belongs_to :repair_type
  belongs_to :merc
  belongs_to :non_maersk
  belongs_to :container_repair_area
  belongs_to :container_damaged_area
end
