class NonMaerskCustomerDetail < ApplicationRecord
  belongs_to :non_maersk_com
  belongs_to :non_maersk_comp
  belongs_to :non_maersk_dam
  belongs_to :non_maersk_event
  belongs_to :non_maersk_rep
end
