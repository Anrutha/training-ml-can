class Container < ApplicationRecord
    has_many :activities
    has_many :temp_activities
    has_many :container_comments
    belongs_to :yard ,foreign_key: :yard_name_id
    belongs_to :customer
    belongs_to :container_type , foreign_key: :container_type_id
    belongs_to :container_height 
    belongs_to :container_length
    has_many :logs

end
