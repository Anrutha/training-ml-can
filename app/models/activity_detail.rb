class ActivityDetail < ApplicationRecord
  belongs_to :activity
  belongs_to :repair_list_detail
end
