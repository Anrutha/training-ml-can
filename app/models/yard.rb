class Yard < ApplicationRecord
    has_many :containers, class_name: 'Container'
end
