class Customer < ApplicationRecord
    enum status: {active: 0 , inactive: 1}
    enum repair_list_type: { merc:0 , non_mearsk:1} 
    has_many :containers
end
