class Activity < ApplicationRecord
  belongs_to :container
  has_many :logs
  has_many :activity_details
  has_many :repair_list_details
  paginates_per 2
  scope :status, -> status { where(status: status) }
  scope :container_id, -> container_id{where(container_id: container_id)}
  scope :activity_type, -> activity_type{where(activity_type: activity_type)}
  scope :date, -> date{where(date: date )}
  scope :yard, ->(yard_name_id) { joins(:container).where(containers: { yard_name_id: yard_name_id }) }
  scope :customer_id, ->(customer_id) { joins(:container).where(containers: { customer_id: customer_id }) }
  scope :status_draft, -> { where(status: ['quote drafted', 'repair draft', 'inspection draft']) }
  scope :status_admin_review_pending,->{where(status:['quote pending','repair done','inspection done'])}
  scope :status_pending_customer_approval,->{where(status:['quote issued'])}
  scope :status_quotes_approved , ->{where(status:['quote approved'])}

  #scope :customer_id, ->(customer_id) { joins(:container).where(containers: { customer_id: customer_id }) }
end
# status=quote_draft
# activity_type=quote
# date=12/2/2023
# yard=1
# customer_id=1
# status_draft=true
# status_admin_review_pending=true
# status_pending_customer_approval=true
# status_quotes_approved=true 
