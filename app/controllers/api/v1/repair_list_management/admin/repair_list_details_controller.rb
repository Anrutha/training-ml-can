require 'csv'
class Api::V1::RepairListManagement::Admin::RepairListDetailsController < ApplicationController
        def upload_csv
          csv_file_path = params[:csv_file_path]
          puts csv_file_path
      
          csv_data = File.read(csv_file_path)
          parsed_csv = CSV.parse(csv_data, headers: true)
      
          # Print the CSV content
          parsed_csv.each do |row|
            puts row.to_hash#["Name"]
          end
      
          #redirect_to your_model_index_path, notice: 'CSV file uploaded and content printed.'
        end
        def upload
          # Get the uploaded file
          file = params[:file]
          process_file(file)
          render json: {success: "Bulk Upload success"},status:201
        end
      
        private
      
        def process_file(file)
          CSV.foreach(file.path, headers: true) do |row|
            #puts row.to_hash["description_1"] , row.to_hash["description_2"]

            create_new_repair_list(row.to_hash)
1


          end
        end
        def create_new_repair_list(value)
          container_damaged_area_id = ContainerDamagedArea.find_by(type_name: value["container_damaged_area"])&.id
          container_repair_area_id = ContainerRepairArea.find_by(type_name: value["container_repair_area"])&.id
          repair_type_id = RepairType.find_by(type_name: value["repair_type"])&.id
          non_maersk_cost_details = {hours:value["repair_type"],material_cost:value["material_cost"]}
          non_maersk_customer_details = {
            description: value["description_1"],
            location: value["location"],
            lgth_qty_area: value["lgth_qty_area"],
            lgth_qty_area_2: value["lgth_qty_area_2"],
            id_source: value["id_source_1"],
            non_maersk_com_id: NonMaerskCom.find_by(type_name: value["non_maersk_com"])&.id,
            non_maersk_comp_id: NonMaerskComp.find_by(type_name: value["non_maersk_comp"])&.id,
            non_maersk_dam_id: NonMaerskDam.find_by(type_name: value["non_maersk_dam"])&.id,
            non_maersk_event_id: NonMaerskEvent.find_by(type_name: value["non_maersk_event"])&.id,
            non_maersk_rep_id: NonMaerskRep.find_by(type_name: value["non_maersk_rep"])&.id



          }
          merc_cost_details = {
            max_mat_cost: value["max_mat_cost"],
            unit_mat_cost: value["unit_mat_cost"],
            hours_per_unit: value["hours_per_unit"],
            max_pieces: value["max_pieces"],
            units: value["units"]

          }
          merc_customer_details = {
            merc_repair_mode_id: MercRepairMode.find_by(mode_number:value["merc_repair_mode"])&.id,
            combined: value["combined"],
            id_source: value["id_source_2"],
            description: value["description_2"],
            merc_mode_number_id: MercModeNumber.find_by(mode_number:value["merc_mode_number"])&.id,
            repair_code: value["repair_code"]
          }

 
          if MercCustomerRelatedDetail.find_by(merc_customer_details)==nil
            merc_customer_detail = MercCustomerRelatedDetail.create(merc_customer_details)
          end

          if MercCostDetail.find_by(merc_cost_details)==nil
            merc_cost_detail = MercCostDetail.create(merc_cost_details)
          end

          if NonMaerskCostDetail.find_by(non_maersk_cost_details)==nil
            non_maersk_cost_detail = NonMaerskCostDetail.create(non_maersk_cost_details)
          end
          if NonMaerskCustomerDetail.find_by(non_maersk_customer_details)==nil
            puts "############## nil"
            non_maersk_customer_detail = NonMaerskCustomerDetail.create(non_maersk_customer_details)
            # non_maersk_customer_detail.save

          end
          merc = {merc_cost_details_id: MercCostDetail.find_by(merc_cost_details)&.id, 
            merc_customer_related_details_id: MercCustomerRelatedDetail.find_by(merc_customer_details)&.id}
          non_maersk = {
            non_maersk_cost_details_id: NonMaerskCostDetail.find_by(non_maersk_cost_details)&.id, 
            non_maersk_customer_details_id: NonMaerskCustomerDetail.find_by(non_maersk_customer_details)&.id
          }
          if Merc.find_by(merc)==nil

            Merc.create(merc)
          end
          if NonMaersk.find_by(non_maersk)==nil

            NonMaersk.create(non_maersk)
          end
          repair_list = {
            repair_type_id: RepairType.find_by(type_name: value["repair_type"])&.id,
            container_repair_area_id: ContainerRepairArea.find_by(type_name: value["container_repair_area"])&.id,
            container_damaged_area_id:ContainerDamagedArea.find_by(type_name: value["container_damaged_area"])&.id,
            merc_id: Merc.find_by(merc)&.id,
            non_maersk_id: NonMaersk.find_by(non_maersk)&.id
          }
          if RepairListDetail.find_by(repair_list)==nil
            RepairListDetail.create(repair_list)
          end
          
          # puts "########### id "
          # puts non_maersk_customer_detail.id

          
        end

end
