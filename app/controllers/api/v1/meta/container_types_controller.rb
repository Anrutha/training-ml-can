class Api::V1::Meta::ContainerTypesController < ApplicationController
    def index 
        container_types = ContainerType.all
        render json: container_types, each_serializer: TypeMetaSerializer  , status:200
    end
end
