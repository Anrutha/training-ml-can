class Api::V1::Meta::ActivityNamesController < ApplicationController
    def index 
        activities = [
            { id: 1, name: "quote" },
            { id: 2, name: "repair" },
            { id: 3, name: "inspection" }
          ]
          
          render json: { activities: activities } , status: 200
        end
    
end
