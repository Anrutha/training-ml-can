class Api::V1::Meta::YardNamesController < ApplicationController
    def index 
        @yards = Yard.all
        render json: @yards , each_serializer:YardIndexSerializer, status:200
    end

end
