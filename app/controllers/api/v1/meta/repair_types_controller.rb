class Api::V1::Meta::RepairTypesController < ApplicationController
    def index 
        repair_types = RepairType.all
        render json: repair_types, each_serializer: TypeMetaSerializer  , status:200
    end
end
