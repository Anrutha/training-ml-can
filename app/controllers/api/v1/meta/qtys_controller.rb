class Api::V1::Meta::QtysController < ApplicationController
    def index 
        qtys = Qty.all
        render json: qtys, each_serializer: MeasurementMetaSerializer  , status:200
    end

end
