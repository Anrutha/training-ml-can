class Api::V1::Meta::ContainerHeightsController < ApplicationController
    def index 
        container_heights = ContainerHeight.all
        render json: container_heights, each_serializer: MeasurementMetaSerializer  , status:200
    end
end
