class Api::V1::Meta::ContainerRepairAreasController < ApplicationController
    def index 
        container_repair_areas = ContainerRepairArea.all
        render json: container_repair_areas, each_serializer: TypeMetaSerializer  , status:200
    end
end
