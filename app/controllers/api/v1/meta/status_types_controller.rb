class Api::V1::Meta::StatusTypesController < ApplicationController
    has_scope :quote, type: :boolean
    has_scope :repair, type: :boolean
    has_scope :inspection, type: :boolean
    def index
        types = apply_scopes(StatusType).all
        render json: types , each_serializer: TypeMetaSerializer , status:200
    end

end
