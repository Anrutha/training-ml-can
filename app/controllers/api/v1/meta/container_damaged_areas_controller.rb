class Api::V1::Meta::ContainerDamagedAreasController < ApplicationController
    def index 
        container_damaged_areas = ContainerDamagedArea.all
        render json: container_damaged_areas, each_serializer: TypeMetaSerializer  , status:200
    end
end
