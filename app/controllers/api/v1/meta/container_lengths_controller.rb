class Api::V1::Meta::ContainerLengthsController < ApplicationController
    def index 
        container_lengths = ContainerLength.all
        render json: container_lengths, each_serializer: MeasurementMetaSerializer  , status:200
    end

end
