class Api::V1::ActivityManagement::Admin::ActivitiesController < ApplicationController
    def index 
        @container = Container.find(params[:id])
        @activities = @container.activities
        render json: @activities
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Container not found' }, status:422       
    end
    def show 
        activity = Activity.find(params[:id])
        render json: activity , status:200
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Activity not found' }, status:422
      end
      def update
        if params[:update_status]=="true"
          begin
          activtiy = Activity.find(params[:id])
    
          if activity.update(update_activity_params)
            render json: activity , status:201
          else
            render json: activity.errors, status: :unprocessable_entity
          end
        rescue ActiveRecord::RecordNotFound
          render json: { error: 'Activity not found' }, status:422
        end 
        elsif params[:add_item]=="true"
          activity_detail = ActivityDetail.new(add_item_params)
          if activity_detail.save
            render json: activity_detail , status:201
          else
            render json: activity_detail.errors , status:422
          end
        end
        rescue ActiveRecord::InvalidForeignKey
          render json: { error: 'Invalid Foreign key ' }, status:422
        print("truee")
      end
      def update_repair_list
        activity_detail = ActivityDetail.find(params[:id])
        if activity_detail.update(update_activity_detail_params)
          render json: activity_detail, status:201
        else
          render json: activity_detail.errors, status: :unprocessable_entity
        end
      rescue ActiveRecord::RecordNotFound
        render json: { error: 'Activity detail not found' }, status:422

      end
      def destroy_activity_details
        activity_detail = ActivityDetail.find(params[:id]).destroy!
        render status: :no_content
      rescue ActiveRecord::RecordNotFound
        render json: { error: 'Activity detail not found' }, status:422

      end

      def update_activity_detail_params
        params.require(:repair_list).permit!
      end


      def update_activity_params
        params.require(:activity).permit!
      end
      def add_item_params
        params.require(:repair_list).permit(
          :activity_id,
          :repair_list_detail_id,
          :qty_id,
          :location,
          :comment,
          :damaged_area_photo_id,
          :repaired_area_photo_id,
          # :container_repair_area_id,
          # :container_damaged_area_id,
          # :repair_type_id

        )
      end


end
