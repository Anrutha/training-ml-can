class Api::V1::ContainerManagement::Admin::ContainersController < ApplicationController
  include WillPaginate::CollectionMethods
  require 'will_paginate/array'
 # require 'kaminari/models/array_extension'
  before_action :authorize_request
  has_scope :status
  has_scope :activity_type
  has_scope :container_id
  has_scope :date 
  has_scope :yar
  has_scope :customer_id
  has_scope :status_draft, type: :boolean
  has_scope :status_admin_review_pending,type: :boolean
  has_scope :status_pending_customer_approval,type: :boolean
  has_scope :status_quotes_approved , type: :boolean

  def index 
    activity_column = ["activity_type","date","status","activity_code"]
    if apply_scopes(Activity).all == Activity.all
      @activities = Activity.select('DISTINCT ON (container_id) *').order("container_id DESC , activities.created_at DESC")
      @current_activity_id = Activity.select('DISTINCT ON (container_id) activities.id').order("container_id DESC , activities.created_at DESC")
      if activity_column.include?(params[:sort_by])
        if params[:sort_order] == "DESC"
          @activities = @activities.sort_by { |activity| activity.send(params[:sort_by]) }.reverse
        else
          @activities = @activities.sort_by { |activity| activity.send(params[:sort_by]) }
        end
      end
      if params[:sort_by]=="container_owner_name"
        if params[:sort_order]== "DESC"
          @activities = Activity.where(id: @current_activity_id).joins(:container).order(container_owner_name:  :DESC )
        else
          @activities = Activity.where(id: @current_activity_id).joins(:container).order(container_owner_name:  :ASC )
        end
      end
      if params[:sort_by]=="customer"
        if params[:sort_order]== "DESC"
          @activities = Activity.where(id: @current_activity_id).joins(container: :customer).order(customer_name:  :DESC )
        else
          @activities = Activity.where(id: @current_activity_id).joins(container: :customer).order(customer_name:  :ASC )
        end
      end
      if params[:sort_by]=="yard"
        if params[:sort_order]== "DESC"
          @activities = Activity.where(id: @current_activity_id).joins(container: :yard).order(yard_name:  :DESC )
        else
          @activities = Activity.where(id: @current_activity_id).joins(container: :yard).order(yard_name:  :ASC )
        end
      end
      @activities = @activities.paginate(page: params[:page], per_page: 10)
      puts "##################"
      puts @activities.as_json
      render json: {
        activities: @activities.map { |activity| ContainerFilterSerializer.new(activity) },
        length: @activities.total_entries
      }, status: 200
    else
      @activities = apply_scopes(Activity).all
    
    if activity_column.include?(params[:sort_by])
      if params[:sort_order] == "DESC"
        @activities = @activities.sort_by { |activity| activity.send(params[:sort_by]) }.reverse
      else
        @activities = @activities.sort_by { |activity| activity.send(params[:sort_by]) }
      end


    end
    if params[:sort_by] == "container_owner_name"
      if params[:sort_order] == "DESC"
        puts "########## before sorting "
        puts @activities.as_json
        @activities = @activities.joins(:container).order(container_owner_name:  :DESC )
        # order_clause = Arel.sql("containers.container_owner_name #{params[:sort_order].upcase}")
        # @activities = @activities.joins(:container).order(order_clause)
        puts "############ after sorting"

        puts @activities.as_json

      else
        #@activities = @activities.joins(:container).order("containers.container_owner_name ASC")
        puts "########## before sorting "
        puts @activities.as_json
        order_clause = Arel.sql("containers.container_owner_name #{params[:sort_order].upcase}")
        #arel : Wrap a known-safe SQL stringfor passing to query methods
        @activities = @activities.joins(:container).order(container_owner_name: :ASC)
        puts "############ after sorting"
        puts @activities.as_json


      end
    end
    if params[:sort_by]== "yard" 
      puts "############## before sorting "
      puts @activties.as_json
      #debugger
      if params[:sort_order]=="DESC"
        @activities = @activities.joins(container: :yard).order('yards.yard_name DESC')
      else
        @activities = @activities.joins(container: :yard).order('yards.yard_name ASC')
      end
      puts "############## after sorting "
      puts @activties.as_json
    end
    if params[:sort_by] == "customer"
      puts "########### before sorting "
      puts @activities.as_json
      if params[:sort_order]=="DESC"
        @activities = @activities.joins(container: :customer).order("customers.customer_name DESC")
      else
        puts "######### entered asc sort"
        @activities = @activities.joins(container: :customer).order("customers.customer_name ASC")
      end
      puts "################ after sorting "
      puts @activities.as_json
    end  
    @activities = @activities.paginate(page: params[:page], per_page: 10)
    puts "##################"
    puts @activities.as_json
    render json: {
      activities: @activities.map { |activity| ContainerFilterSerializer.new(activity) },
      length: @activities.total_entries
    }, status: 200
    end 
    end
    def quote_approve
      activity_ids = get_activity_ids["ids"]
      activities = Activity.where(id: activity_ids)  # Retrieve the containers from the database
      activities.each do |activity|
        activity.update(status: "quote approved")  # Change the status of each container
    # rescue ActiveRecord::RecordNotFound
    #   puts "##########"
    #     render json: { error: 'Activity not found' }, status:422
      end
    end

    def create 
        container = Container.new(create_container_params)

        if container.save 
        render json: container , status:201
        else
            render json: container.errors , status:422
        end
      rescue ActiveRecord::RecordNotUnique
        render json: {error: "Container number not unique"},status:422
      puts "create ####"

    end
    def show
        container = Container.find(params[:id])
        puts container.as_json
        #puts object.yard.name ifobject.yar
        render json: container , serializer: ContainerShowSerializer , status:200
      rescue ActiveRecord::RecordNotFound
        render json: { error: 'Container not found' }, status:422


    end
    def destroy
        container = Container.find(params[:id]).destroy!
        render status: :no_content
    end
    def update
        container = Container.find(params[:id])

    
        if container.update(update_container_params)
          render json: container , status:201
        else
          render json: container.errors, status: :unprocessable_entity
        end
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Container not found' }, status:422
    end

    def create_container_params
        params.require(:containers).permit(
          :container_num,
          :container_owner_name,
          :submitter_initials,
          :container_manufacture_year,
          :location,
          :comments,
          :door_photo_including_container_number_id,
          :left_side_photo_id,
          :right_side_photo_id,
          :front_side_photo_id,
          :interior_photo_id,
          :underside_photo_id,
          :roof_photo_id,
          :csc_plate_number_id,
          :yard_name_id,
          :customer_id,
          :container_height_id,
          :container_type_id,
          :container_length_id
        )
      end

      def update_container_params
        params.require(:container).permit!
      end
      def get_activity_ids
        params.require(:activities).permit!
      end
      def authorize_request
        # Check the authorization header for the bearer token
        authorization_header = request.headers['Authorization']
        if authorization_header && authorization_header.start_with?('bearer ')
          bearer_token = authorization_header[7..-1]
          puts "############### auth request"
          puts bearer_token
          puts '############## '
          render json: { error: 'Unauthorized' }, status: 401 unless token_valid?(bearer_token)
        else
          render json: { error: 'Unauthorized' }, status: 401
        end
      end
      def token_valid?(token)
        # puts "######### valid token "
        # puts token 
        if token=="qwerty"
          puts "############# token_valid"
          true
        else
          false
        end

      end

end

