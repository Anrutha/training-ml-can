class Api::V1::LogManagement::Admin::LogsController < ApplicationController
    def index 
          @logs = Log.where(container: params[:id])

          if @logs.empty? 
            render json: { error: 'Container not found' }, status: 422
          else
            render json: @logs, each_serializer: LogIndexSerializer, status: 200
          end  
    
      end
      
    def create
        value = create_log_params
        done_by = value[:done_by]
        activity_id = value[:activity_id]
        time = DateTime.now
        status_changed_from = value[:status_changed_from]
        status_changed_to = value[:status_changed_to]
        container_id = Activity.find(activity_id)["container_id"]

        log = Log.new(
            done_by: done_by,
            activity_id: activity_id,
            container_id: container_id,
            time: time,
            status_changed_from: status_changed_from,
            status_changed_to: status_changed_to
          )
      
          if log.save
            render json: { log: log }, status: :created
          else
            render json:{message: log.errors },status:422
          end
    rescue ActiveRecord::RecordNotFound
            render json: { error: 'Activity not found' }, status:422


    end
    def create_log_params
        params.require(:log).permit(
          :status_changed_from,
          :status_changed_to,
          :done_by,
          :activity_id
        )
      end
end
