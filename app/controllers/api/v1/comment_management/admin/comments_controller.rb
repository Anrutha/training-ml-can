class Api::V1::CommentManagement::Admin::CommentsController < ApplicationController
    def index 
        @container = Container.find(params[:id])
        @comments = @container.container_comments
        render json: @comments
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Container not found' }, status:422    
    end
    def create
        @container = Container.find(params[:id])
        @comment = @container.container_comments.build(comment_params)
        @comment.date = Date.current
    
      
        if @comment.save
            render json: { success: true, comment: @comment }, status: :created
        else
            render json: { success: false, errors: @comment.errors }, status: :unprocessable_entity
        end
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Container not found' }, status:422
    end
    def update
        comment = ContainerComment.find(params[:id])

    
        if comment.update(update_comment_params)
          render json: comment , status:201
        else
          render json: comment.errors, status: :unprocessable_entity
        end
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Comment not found' }, status:422
    end
    def destroy
        comment = ContainerComment.find(params[:id]).destroy!
        render status: :no_content
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Comment  not found' }, status:422
    end

      
    private
      
    def comment_params
        params.require(:comment).permit(:comment, :commented_by)
    end
    def update_comment_params
        params.require(:comment).permit!
      end

end
