class Api::V1::CustomerManagement::Admin::CustomersController < ApplicationController
    def index
        customer = Customer.all
        render json: customer , each_serializer: CustomerIndexSerializer , status:200
      end
    
      def create
        if customer_params[:email].blank? or customer_params[:password].blank?
          render status:422
        else
          customer = Customer.new(customer_params)
    
          if customer.save
            render json: customer, status: :created
          else
            render json: customer.errors, status: :unprocessable_entity
          end
        end
      
    end
    def meta 
      customer = Customer.all
      render json: customer , each_serializer: CustomerMetaSerializer , status:200
    end 

      def show 
        customer = Customer.find(params[:id])
        #puts customer.as_json
        render json: customer , Serializer: CustomerShowSerializer #, status:200
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Customer not found' }, status:422
      end
 

      def destroy
        customer = Customer.find(params[:id]).destroy!
        render status: :no_content  
    rescue ActiveRecord::RecordNotFound
        render json: { error: 'Customer not found' }, status: :not_found      
      end
      def update
        customer = Customer.find(params[:id])
    
        if customer.update(update_customer_params)
          render json: customer , status:201
        else
          render json: customer.errors, status: :unprocessable_entity
        end
      rescue ActiveRecord::RecordNotFound
        render json: { error: 'Customer not found' }, status:422
      end
    

    
      private
    
      def customer_params
        params.require(:customer).permit(:customer_name, :email, :owner_name, :billing_name, :hourly_rate, :gst, :pst, :city_id, :address, :postal_code, :password,:province, :repair_list_type, :status)

      end
      def update_customer_params
        params.require(:customer).permit!
      end
end
