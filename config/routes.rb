Rails.application.routes.draw do
  
  use_doorkeeper
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
#  get 'api/v1/container_management/admin/containers', to: 'containers#index', as: 'admin_containers'
  get 'anrutha-mlcan/api/v1/container_management/admin/containers', to: 'api/v1/container_management/admin/containers#index'


  namespace :api do
  	namespace :v1 do
      namespace :user_management do
        namespace :user do
          post 'auth/signup'
          delete 'auth/logout'
          post 'auth/login'
          post 'auth/forgot_password'
          get 'auth/view'
          post 'auth/validate_reset_password_token'
          # put 'auth/password'
        end
      end
  		resources :items, except: [:new, :edit]
    end
  end
  namespace :api do
  	namespace :v1 do
      get '/meta/customer',to:'customer_management/admin/customers#meta'
      namespace :meta do
        get 'yard' , to: 'yard_names#index'
        
        get 'container_length' , to:'container_lengths#index'
        get 'container_height' , to:'container_heights#index'
        get 'container_type' , to:'container_types#index'
        get 'status_type' , to:'status_types#index'
        get 'activity', to:'activity_names#index'
        get 'repair_area',to:'container_repair_areas#index'
        get 'damaged_area',to:'container_damaged_areas#index'
        get 'qty', to:'qtys#index'
        get 'repair_type',to:'repair_types#index'
        end
      end
    end

  #resources :containers, only: :index
  #get 'container/index'
  namespace :api do
    namespace :v1 do
      namespace :container_management do
        namespace :admin do 
        resources :containers
        #get 'containers', to:'containers#index' 
        #get 'containers/filter', to:'containers#filter'
        post 'containers', to:'containers#create'
        post 'containers/quote_approve',to: 'containers#quote_approve'
        get 'containers/:id', to:'containers#show'
        patch 'containers/:id' , to:'containers#update'
        delete 'containers/:id', to:'containers#destroy'
      end
    end
  end
end
namespace :api do
  namespace :v1 do
    namespace :customer_management do
      namespace :admin do 
      resources :customers
      get 'customers', to:'customers#index' 
      #get 'customers/' , to: 'customers#filter'
      post 'customers', to:'customers#create'
      patch 'customers/:id' , to:'customers#update'
      get 'customers/:id', to:'customers#show'
      delete 'customers/:id', to:'customers#destroy'
    end
  end
end
end

namespace :api do
  namespace :v1 do
    namespace :repair_list_management do
      namespace :admin do
      #get 'customers/' , to: 'customers#filter'
      post 'repair_list_details/bulk_upload', to:'repair_list_details#upload_csv'
      post 'repair_list_details/file_upload' , to:'repair_list_details#upload'
    end
  end
end
end
namespace :api do
  namespace :v1 do
    namespace :activity_management do
      namespace :admin do 
      get 'containers/:id/activities', to:'activities#index' 
      get 'activities/:id' , to:'activities#show'
      post 'containers/:id/activities', to:'activities#create'
      put 'activities/:id' , to:'activities#update'
      put 'activity_details/:id',to:'activities#update_repair_list'
      delete 'activity_details/:id' , to:'activities#destroy_activity_details'
      #get 'customers/:id', to:'ccustomers#show'
      #delete 'customers/:id', to:'customers#destroy'
    end
  end
end
end 

namespace :api do
  namespace :v1 do
    namespace :comment_management do
      namespace :admin do 
      get 'containers/:id/comments', to:'comments#index' 
      post 'containers/:id/comments', to:'comments#create'
      delete 'comments/:id', to:'comments#destroy'
      put 'comments/:id', to:'comments#update'
      #get 'activities/:id' , to:'activities#show'
      #post 'containers/:id/activities', to:'activities#create'
      #get 'customers/:id', to:'ccustomers#show'
      #delete 'customers/:id', to:'customers#destroy'
    end
  end
end
end
namespace :api do
  namespace :v1 do
    namespace :log_management do
      namespace :admin do 
      get 'containers/:id/logs', to:'logs#index' 
      post 'logs', to:'logs#create'
      #post 'containers/:id/comments', to:'comments#create'
      #get 'activities/:id' , to:'activities#show'
      #post 'containers/:id/activities', to:'activities#create'
      #get 'customers/:id', to:'ccustomers#show'
      #delete 'customers/:id', to:'customers#destroy'
    end
  end
end
end
  

 
    

end
