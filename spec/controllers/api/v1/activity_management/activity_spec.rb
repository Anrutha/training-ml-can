require 'rails_helper'
require 'database_cleaner'
RSpec.describe "Api::V1::Activity", type: :request do
    let!(:yard1){create(:yard)}
    let!(:type1){create(:container_type)}
    let!(:height1){create(:container_height)}
    let!(:length1){create(:container_length)}
    let!(:customer){create(:customer)}
    let!(:container1){create(:container)}
    describe "index" do
        context "container_id does not exist " do 
            let!(:activity){create(:activity)}
            before {get "/api/v1/activity_management/admin/containers/2/activities"} 
            it "Unprocessable Content" do
                expect(response).to have_http_status(422)
            end

        end
        context "container_id exists " do
            let!(:activity) { create(:activity, container: container1) }
            before {get "/api/v1/activity_management/admin/containers/#{container1.id}/activities"}
            it "displays activites of container " do
                expect(response).to have_http_status(200)
            end
        end

    end
    describe "show" do
        context "activity id does not exist " do
            before { get "/api/v1/activity_management/admin/activities/1"} 
            it "Unprocessable Content" do 
                expect(response).to have_http_status(422)
            end
        end
        context "activity id exist " do
            let!(:activity) { create(:activity, container: container1) }
            before {get "/api/v1/activity_management/admin/activities/#{activity.id}"}
            it "displays details of activity " do
                expect(response).to have_http_status(200)
            end
        end
    end
end
