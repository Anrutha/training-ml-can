require 'rails_helper'

RSpec.describe "Api::V1::StatusTypesController", type: :request do    
    describe 'index' do
        let!(:status_types) do
            [
              create(:status_type, id: 1, type_name: "quote_draft"),
              create(:status_type, id: 2, type_name: "quote_pending"),
              create(:status_type, id: 3, type_name: "quote_issued"),
              create(:status_type, id: 4, type_name: "quote_approved"),
              create(:status_type, id: 5, type_name: "repair_draft"),
              create(:status_type, id: 6, type_name: "repair_done"),
              create(:status_type, id: 7, type_name: "inspection_done"),
              create(:status_type, id: 8, type_name: "inspection_draft"),
              create(:status_type, id: 9, type_name: "idle")
            ]
          end
        context 'no query params' do

            before {get "/api/v1/meta/status_type"}
            it "displays all status types" do
                response_body = JSON.parse(response.body)
                expect(response_body['status_types'].count).to eq(9)
                expect(response).to have_http_status(200)
            end
            
        end
        context 'quote=true' do

            before {get "/api/v1/meta/status_type?quote=true"}
            it "displays status of quote" do
                response_body = JSON.parse(response.body)
                expect(response_body['status_types'].count).to eq(4)
                expect(response).to have_http_status(200)
            end
            
        end
        context 'repair=true' do

            before {get "/api/v1/meta/status_type?repair=true"}
            it "displays status of repair" do
                response_body = JSON.parse(response.body)
                expect(response_body['status_types'].count).to eq(2)
                expect(response).to have_http_status(200)
            end
            
        end
        context 'inspection=true' do

            before {get "/api/v1/meta/status_type?inspection=true"}
            it "displays all status types" do
                response_body = JSON.parse(response.body)
                expect(response_body['status_types'].count).to eq(2)
                expect(response).to have_http_status(200)
            end
            
        end

    end
end
