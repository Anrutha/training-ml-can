require 'rails_helper'

RSpec.describe "Api::V1::Customer", type: :request do
    describe 'index' do
       
        context 'displays all containers current activity' do
            let!(:customer){create(:customer)}
            before {get '/api/v1/customer_management/admin/customers'}
            it "success" do
                expect(response).to have_http_status(200)
              end
            end 
        end
    describe 'show' do
        
        
        context 'customer id exists' do
            let!(:customer){create(:customer)}
            
            before {get "/api/v1/customer_management/admin/customers/#{customer.id}"}
            it "shows details of customer" do
                puts '##'
                puts customer.id
                puts "##"
                expect(response).to have_http_status(200)
            end

            
        end
        context "customer id does not exists" do
            before {get "/api/v1/customer_management/admin/customers/1"}
            it "record not found " do
                expect(response).to have_http_status(422)
            end
        end
        
    end
    describe "create" do 
        context "creates new customer" do
            let(:create_attributes){
                {
                    "customer": {
                        "customer_name": "coast",
                        "email": "jack.ellis@mail.com",
                        "owner_name": "jack",
                        "billing_name": "Coast Shipping company ",
                        "hourly_rate": "10$",
                        "gst": "10%",
                        "pst": "0%",
                        "city_id": 1,
                        "address": "234 Posrt Camp Road",
                        "postal_code": "value",
                        "password": "112334",
                        "province": "BC",
                        "repair_list_type": "merc",
                        "status": "active"
                    }
                }
             }
            before {post "/api/v1/customer_management/admin/customers" ,params: create_attributes, as: :json} 
            it "success" do
                expect(response).to have_http_status(201)
            end        
        end
        context "parameter missing " do
            let(:create_attributes){
                {
                    "customer": {

                        "customer_name": "jack",
                        "owner_name": "jack",
                        "billing_name": "Coast Shipping company ",
                        "hourly_rate": "10$",
                        "gst": "10%",
                        "pst": "0%",
                        "city_id": 1,
                        "address": "234 Posrt Camp Road",
                        "postal_code": "value",
                        "province": "BC",
                        "repair_list_type": "merc",
                        "password": "112334",
                        "status": "active"
                    }
                }
             }
            before {post "/api/v1/customer_management/admin/customers" ,params: create_attributes, as: :json}
            it "new customer is not created " do
                expect(response).to have_http_status(422)
            end
            
        end
    end
    describe "update" do
        context "customer id exists" do
            let!(:customer){create(:customer)}
            let!(:update_attributes){
                {
                    "customer":{
                        "email": "jack@gmail.com"
                    }
                }
            }
            before {patch "/api/v1/customer_management/admin/customers/#{customer.id}" , params: update_attributes, as: :json}
            it "email is updated " do
                expect(response).to have_http_status(201)
            end

        end 
        context "customer id do not exist" do
            let!(:update_attributes){
                {
                    "customer":
                    {
                        "email": "abc@gmail.com"
                    }
                }
            }
            before {patch "/api/v1/customer_management/admin/customers/1" , params: update_attributes, as: :json}
            it "does not updates" do
                expect(response).to have_http_status(422)
            end
        end
    end


end

    

