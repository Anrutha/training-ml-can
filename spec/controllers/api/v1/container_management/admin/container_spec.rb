require 'rails_helper'
require 'database_cleaner'
RSpec.describe "Api::V1::Container", type: :request do
    describe "index" do
        let!(:yard1){create(:yard,yard_name: "Yard A")}
        let!(:type1){create(:container_type,type_name:"type 3")}
        let!(:height1){create(:container_height,measurement:"18")}
        let!(:length1){create(:container_length,measurement:"16")}
        let!(:yard2){create(:yard,yard_name: "Yard B")}
        let!(:type2){create(:container_type,type_name:"type 4")}
        let!(:height2){create(:container_height,measurement:"16")}
        let!(:length2){create(:container_length,measurement:"18")}
        let!(:customer1){create(:customer,customer_name: "Mike")}
        let!(:customer2){create(:customer,customer_name: "Abby")}
        let!(:container1){create(:container,container_num: "CON001",yard: yard1,container_type: type1,container_height: height1,container_length: length1,customer: customer1)}
        let!(:container2){create(:container,container_num: "CON002",yard: yard2,container_type: type2,container_height: height2,container_length: length2,customer: customer2)}

        let!(:activity1){create(:activity,
            container_id: container1.id,
            activity_type: "quote form",
            activity_code: "QF001",
            status: "quote drafted",
            date: "12/3/2022",
            current_user: "John Doe"
          )}
          let!(:activity2){create(:activity,
            container_id: container1.id,
            activity_type: "quote form",
            activity_code: "QF001",
            status: "quote accepted",
            date: "13/3/2022",
            current_user: "John Doe"
          )}
          let!(:activity3){create(:activity,
            container_id: container2.id,
            activity_type: "quote form",
            activity_code: "QF002",
            status: "quote approved",
            date: "13/3/2022",
            current_user: "John Doe"
          )}
          let!(:activity4){create(:activity,
            container_id: container2.id,
            activity_type: "quote form",
            activity_code: "QF002",
            status: "quote issued",
            date: "15/3/2022",
            current_user: "Mika")}
           let!(:headers){
            
            {
                "Authorization": "bearer qwerty"
            }
        
              }
        context "incorrect bearer token " do
            before { get "/api/v1/container_management/admin/containers"}
            it "unauthorized" do 
                expect(response).to have_http_status(401)
            end 
        end
        
        context "displays details of containers current activity" do

         before do

            get "/api/v1/container_management/admin/containers" , headers: headers
            end
            it " sucess" do
                response_body = JSON.parse(response.body)
                puts response_body
                expect(response_body['activities'].count).to eq(2)
                expect(response).to have_http_status(200)
            end
        end
        context "displays details of containers with status as draft " do
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    

            before {get "/api/v1/container_management/admin/containers?status_draft=true",headers: headers}
               it " sucess" do
                   response_body = JSON.parse(response.body)
                   puts response_body
                   expect(response_body['activities'].count).to eq(1)
                   expect(response).to have_http_status(200)
               end
           end
           context "displays details of containers with yard_id = 1 " do
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    

            before {get "/api/v1/container_management/admin/containers?yard=#{yard1.id}",headers: headers}
               it " sucess" do
                   response_body = JSON.parse(response.body)
                   puts "######### spec index"
                   puts yard1.id 
                   puts yard2.id
                   puts container1.id
                   puts response_body
                   expect(response_body['activities'].count).to eq(2)
                   expect(response).to have_http_status(200)
               end
           end
           context "sort by yard " do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=yard&sort_order=DESC", headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                expect(response_json[0]["yard"]).to eq("Yard B")
                
            end
        end
        context "sort by status" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=status&sort_order=DESC" , headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json[0]["customer"]).to eq("Abby")
                
            end
        end
        context "sort by status" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=status&sort_order=DESC", headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json[0]["customer"]).to eq("Abby")
                
            end
        end
        context "sort by customer" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=customer&sort_order=DESC" , headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json[1]["customer"]).to eq("Abby")
                
            end
        end
        context "sort by date" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=date&sort_order=DESC" , headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json[0]["container_id"]).to eq( container2.id)
                
            end
        end
        context "sort by date" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=date&sort_order=ASC" , headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json[1]["container_id"]).to eq( container2.id)
                
            end
        end
        context "sort by container_owner_name" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=container_owner_name&status_draft=true&sort_order=DESC", headers:headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json.length ).to eq(1)
                
            end
        end
        context "sort by container_owner_name" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?sort_by=container_owner_name&sort_order=DESC",headers: headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json.length ).to eq(2)
                
            end
        end
        context "sort by container_owner_name" do 
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers?yard=1&sort_by=container_owner_name&sort_order=ASC",headers:headers}
            it "success " do
                response_json = JSON.parse(response.body)["activities"]
                puts response_json
                expect(response_json.length ).to eq(2)
                
            end
        end
    end
    describe "show" do
        context " container id exists" do
            let!(:yard1){create(:yard,yard_name: "8346")}
            let!(:type1){create(:container_type,type_name:"type 3")}
            let!(:height1){create(:container_height,measurement:"18")}
            let!(:length1){create(:container_length,measurement:"16")}
            let!(:customer){create(:customer)}
            let!(:container){create(:container,yard: yard1,container_type: type1,container_height: height1,container_length: length1,customer: customer)}
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
    
            before {get "/api/v1/container_management/admin/containers/#{container.id}" , headers: headers}
            it "displays details" do
                expect(response).to have_http_status(200)
            end
        end
        context "container id does not exist" do
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
            
            before {get "/api/v1/container_management/admin/containers/1" , headers: headers}
            it "do not display " do
                expect(response).to have_http_status(422)
            end
        end
    end
    describe "create" do 
        
        context "container num is unique " do
            let!(:yard1){create(:yard,yard_name: "8346")}
            let!(:type1){create(:container_type,type_name:"type 3")}
            let!(:height1){create(:container_height,measurement:"18")}
            let!(:length1){create(:container_length,measurement:"16")}
            let!(:customer){create(:customer)}         
            let!(:create_attributes){
                {
                    "containers": {
                      "container_num": "ABC123",
                      "container_owner_name": "Container Owner",
                      "submitter_initials": "SI",
                      "container_manufacture_year": 2022,
                      "location": "Container Location",
                      "comments": "Some comments",
                      "door_photo_including_container_number_id": 1,
                      "left_side_photo_id": 2,
                      "right_side_photo_id": 3,
                      "front_side_photo_id": 4,
                      "interior_photo_id": 5,
                      "underside_photo_id": 6,
                      "roof_photo_id": 7,
                      "csc_plate_number_id": 8,
                      "yard_name_id": yard1.id,
                      "customer_id": customer.id,
                      "container_height_id": height1.id,
                      "container_type_id": type1.id,
                      "container_length_id": length1.id
                    }
                  }
                  
            }
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
            before {post "/api/v1/container_management/admin/containers" , headers: headers , params: create_attributes , as: :json}
            it "creates new container" do
                expect(response).to have_http_status(201)
            end
        end
        context "container num is not unique" do
            let!(:yard1){create(:yard,yard_name: "8346")}
            let!(:type1){create(:container_type,type_name:"type 3")}
            let!(:height1){create(:container_height,measurement:"18")}
            let!(:length1){create(:container_length,measurement:"16")}
            let!(:customer){create(:customer)}
            let!(:container){create(:container,yard: yard1,container_type: type1,container_height: height1,container_length: length1,customer: customer)}
         
            let!(:create_attributes){
                {
                    "containers": {
                      "container_num": "CON001",
                      "container_owner_name": "Container Owner",
                      "submitter_initials": "SI",
                      "container_manufacture_year": 2022,
                      "location": "Container Location",
                      "comments": "Some comments",
                      "door_photo_including_container_number_id": 1,
                      "left_side_photo_id": 2,
                      "right_side_photo_id": 3,
                      "front_side_photo_id": 4,
                      "interior_photo_id": 5,
                      "underside_photo_id": 6,
                      "roof_photo_id": 7,
                      "csc_plate_number_id": 8,
                      "yard_name_id": 1,
                      "customer_id": 2,
                      "container_height_id":1,
                      "container_type_id": 1,
                      "container_length_id": 1
                    }
                  }
                }
                let!(:headers){
            
                    {
                        "Authorization": "bearer qwerty"
                    }
                
                      }
                before {post "/api/v1/container_management/admin/containers" ,headers: headers ,  params: create_attributes , as: :json}
                it "do not creates new container" do
                    expect(response).to have_http_status(422)
                end
                
            
        end
    end
    describe "update" do
        context "container id exist" do
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
            let!(:yard1){create(:yard,yard_name: "Yard A")}
            let!(:yard2){create(:yard,yard_name:"Yard B")}
            let!(:type1){create(:container_type,type_name:"type 3")}
            let!(:height1){create(:container_height,measurement:"18")}
            let!(:length1){create(:container_length,measurement:"16")}
            let!(:customer){create(:customer)}
            let!(:container){create(:container,yard: yard1,container_type: type1,container_height: height1,container_length: length1,customer: customer)}
         
            let!(:update_attributes){
                {
                    "container":{
                        "yard_name_id": yard2.id
                    }
                }
            }
            before {patch "/api/v1/container_management/admin/containers/#{container.id}" , headers: headers , params: update_attributes , as: :json}
            it "updates yard id " do
                expect(response).to have_http_status(201)
            end
        end
        context "container id does not exist" do
            let!(:update_attributes){
                {
                    "container":{
                        "yard_name_id":2
                    }
                }
            }
            let!(:headers){
            
                {
                    "Authorization": "bearer qwerty"
                }
            
                  }
            before {patch "/api/v1/container_management/admin/containers/1" ,headers: headers ,  params: update_attributes , as: :json}
            it "do not updates" do
                expect(response).to have_http_status(422)
            end
        end
    end

end
