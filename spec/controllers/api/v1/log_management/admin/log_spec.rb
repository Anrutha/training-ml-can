require 'rails_helper'

RSpec.describe "Api::V1::Log", type: :request do
    describe 'index' do
        context "Container id does not exist " do 
            before {get '/api/v1/log_management/admin/containers/1/logs'}
            it "no logs displayed" do
                expect(response).to have_http_status(422)

            end
        end
        context "Container id exist " do 
            let!(:yard1){create(:yard)}
            let!(:type1){create(:container_type)}
            let!(:height1){create(:container_height)}
            let!(:length1){create(:container_length)}
            let!(:customer){create(:customer)}
            let!(:container1){create(:container)}
            let!(:activity1){create(:activity ,activity_type:"quote",activity_code:"QF001",status:"quote draft",date:"12/3/2023",container_id: 1 )}
            let!(:log1){create(:log,time:DateTime.now,status_changed_from: "quote draft",status_changed_to:"quote pending",done_by:"John",activity_id: 1 , container_id: 1)}
            let!(:log2){create(:log,time:DateTime.now,status_changed_from: "quote pending",status_changed_to:"quote issued",done_by:"John",activity_id: 1 , container_id: 1)}
            let!(:log3){create(:log,time:DateTime.now,status_changed_from: "quote issued",status_changed_to:"quote approved",done_by:"John",activity_id: 1 , container_id: 1)}

            before {get "/api/v1/log_management/admin/containers/#{container1.id}/logs"}
            it "displays logs of containerId 1" do
                expect(response).to have_http_status(200)
                response_json = JSON.parse(response.body)
                expect(response_json["logs"].length).to eq(3)


                
            end
        end

    end
    describe "create" do
        context "activity id do not exist " do 
            let!(:create_log_attributes){
                {
                    "log":{
                        "status_changed_from":"quote issued",
                        "status_changed_to":"quote_approved",
                        "done_by":"Mike",
                        "activity_id":1
                    }
                }
            }
            before { post "/api/v1/log_management/admin/logs",params: create_log_attributes, as: :json} 
            it "new log is not added" do
                expect(response).to have_http_status(422)
            end
        

        end 
        context "activity id exist " do 

            let!(:yard1){create(:yard)}
            let!(:type1){create(:container_type)}
            let!(:height1){create(:container_height)}
            let!(:length1){create(:container_length)}
            let!(:customer){create(:customer)}
            let!(:container1){create(:container,customer_id: customer.id,yard_name_id: yard1.id , container_type_id:type1.id , container_height_id:height1.id,container_length_id: length1.id )}
            let!(:activity1){create(:activity ,activity_type:"quote",activity_code:"QF001",status:"quote draft",date:"12/3/2023",container_id: container1.id )}
            let!(:create_log_attributes){
                {
                    "log":{
                        "status_changed_from":"quote issued",
                        "status_changed_to":"quote_approved",
                        "done_by":"Mike",
                        "activity_id":activity1.id
                    }
                }
            }
            before { post "/api/v1/log_management/admin/logs",params: create_log_attributes, as: :json} 

            it "new log is  added" do
                expect(response).to have_http_status(201)
            end
        end
        

    end
end
