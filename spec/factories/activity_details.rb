FactoryBot.define do
  factory :activity_detail do
    activity { nil }
    repair_list_detail { nil }
    qty { "MyString" }
    location { "MyString" }
    comment { "MyString" }
    damaged_area_phot_id { 1 }
    repaired_area_photo_id { 1 }
  end
end
