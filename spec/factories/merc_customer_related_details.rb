FactoryBot.define do
  factory :merc_customer_related_detail do
    merc_repair_mode { nil }
    combined { "MyString" }
    id_source { "MyString" }
    description { "MyString" }
    merc_mode_number { nil }
    repair_code { "MyString" }
  end
end
