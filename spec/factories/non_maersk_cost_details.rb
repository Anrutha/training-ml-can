FactoryBot.define do
  factory :non_maersk_cost_detail do
    hours { "MyString" }
    material_cost { "MyString" }
  end
end
