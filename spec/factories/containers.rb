FactoryBot.define do
  factory :container do
    container_num {"CON001"}
    container_owner_name {"jack"}
    submitter_initials {"AC"}
    container_manufacture_year {2001}
    location {"India"}
    comments {"some comments"}
    door_photo_including_container_number_id{1}
    left_side_photo_id{1}
    right_side_photo_id {2}
    front_side_photo_id {3}
    interior_photo_id {3}
    underside_photo_id{1}
    roof_photo_id{1}
    csc_plate_number_id {2}
    yard_name_id{1}
    customer_id{1}
    container_height_id{1}
    container_type_id {1}
    container_length_id {1}
  end
end
