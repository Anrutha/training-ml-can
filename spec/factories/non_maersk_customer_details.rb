FactoryBot.define do
  factory :non_maersk_customer_detail do
    description { "MyString" }
    location { "MyString" }
    lgth_qty_area { "MyString" }
    lgth_qty_area_2 { "MyString" }
    id_source { "MyString" }
    non_maersk_com { nil }
    non_maersk_comp { nil }
    non_maersk_dam { nil }
    non_maersk_event { nil }
    non_maersk_rep { nil }
  end
end
