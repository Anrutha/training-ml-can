FactoryBot.define do
  factory :merc_cost_detail do
    max_mat_cost { "MyString" }
    unit_mat_cost { "MyString" }
    hours_per_unit { "MyString" }
    max_pieces { "MyString" }
    units { "MyString" }
  end
end
