FactoryBot.define do
  factory :customer do
    email { "john@gmail.com" }
    owner_name {"john A" }
    billing_name { "john A" }
    hourly_rate { '10.00' }
    gst { '5%' }
    pst { '7%' }
    city_id { 1 }
    address { "123 port bancouver road" }
    province { "BC" }
    postal_code { "1234" }
    password { 'password123' }
    status { 0 }
    repair_list_type { 0 }
  end
end
