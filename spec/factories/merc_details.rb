FactoryBot.define do
  factory :merc_detail do
    merc_customer_related_detail { nil }
    merc_cost_detail { nil }
  end
end
