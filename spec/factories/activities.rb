FactoryBot.define do
  factory :activity do
    activity_type { "quote form" }
    activity_code { "QF001" }
    status { "quote draft" }
    date { "2023-06-08" }
    current_user { "John" }
    container_id { 1 }
    
  end
end
