class AddColumnsToYards < ActiveRecord::Migration[6.0]
  def change
    add_column :yards, :yard_name, :string
    add_index :yards, :yard_name, unique: true
  end
end
