class CreateRepairListDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :repair_list_details do |t|
      t.references :repair_type, null: false, foreign_key: true
      t.references :merc, null: false, foreign_key: true
      t.references :non_maersk, null: false, foreign_key: true
      t.references :container_repair_area, null: false, foreign_key: true
      t.references :container_damaged_area, null: false, foreign_key: true

      t.timestamps
    end
  end
end
