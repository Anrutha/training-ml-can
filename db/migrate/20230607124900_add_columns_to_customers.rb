class AddColumnsToCustomers < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :customer_name, :string
    add_column :customers, :email, :string
    add_column :customers, :owner_name, :string
    add_column :customers, :billing_name, :string
    add_column :customers, :hourly_rate, :string
    add_column :customers, :gst, :string
    add_column :customers, :pst, :string
    add_column :customers, :city_id, :integer
    add_column :customers, :address, :string
    add_column :customers, :province, :string
    add_column :customers, :postal_code, :string
    add_column :customers, :password, :string
  end
end
