class AddColumnsToContainers < ActiveRecord::Migration[6.0]
  def change
    add_column :containers, :container_num, :string
    add_column :containers, :container_owner_name, :string
    add_column :containers, :submitter_initials, :string
    add_column :containers, :container_manufacture_year, :integer
    add_column :containers, :location, :string
    add_column :containers, :comments, :text
    add_column :containers, :door_photo_including_container_number_id, :integer
    add_column :containers, :left_side_photo_id, :integer
    add_column :containers, :right_side_photo_id, :integer
    add_column :containers, :front_side_photo_id, :integer
    add_column :containers, :interior_photo_id, :integer
    add_column :containers, :underside_photo_id, :integer
    add_column :containers, :roof_photo_id, :integer
    add_column :containers, :csc_plate_number_id, :integer
    add_column :containers, :container_type_id, :integer
  end
end
