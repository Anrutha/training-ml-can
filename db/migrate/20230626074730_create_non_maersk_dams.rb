class CreateNonMaerskDams < ActiveRecord::Migration[6.0]
  def change
    create_table :non_maersk_dams do |t|
      t.string :type_name

      t.timestamps
    end
  end
end
