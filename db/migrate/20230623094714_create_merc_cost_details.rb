class CreateMercCostDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :merc_cost_details do |t|
      t.string :max_mat_cost
      t.string :unit_mat_cost
      t.string :hours_per_unit
      t.string :max_pieces
      t.string :units

      t.timestamps
    end
  end
end
