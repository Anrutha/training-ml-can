class CreateActivityDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :activity_details do |t|
      t.references :activity, null: false, foreign_key: true ,  column: :foreign_key_column
      t.references :repair_list_detail, null: false, foreign_key: true ,  column: :foreign_key_column
      t.string :qty
      t.string :location
      t.string :comment
      t.integer :damaged_area_phot_id
      t.integer :repaired_area_photo_id

      t.timestamps
    end
  end
end
