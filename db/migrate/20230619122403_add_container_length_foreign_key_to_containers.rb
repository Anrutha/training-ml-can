class AddContainerLengthForeignKeyToContainers < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :containers, :container_lengths, column: :container_length_id

  end
end
