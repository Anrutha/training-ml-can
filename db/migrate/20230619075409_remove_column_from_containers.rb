class RemoveColumnFromContainers < ActiveRecord::Migration[6.0]
  def change
    remove_column :containers, :yard_name_id, :integer
  end
end
