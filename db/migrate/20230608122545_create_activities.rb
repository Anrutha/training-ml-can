class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.string :activity_type
      t.string :activity_code
      t.string :status
      t.date :date
      t.string :current_user
      t.references :container, null: false, foreign_key: true

      t.timestamps
    end
  end
end
