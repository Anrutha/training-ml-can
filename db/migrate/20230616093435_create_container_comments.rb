class CreateContainerComments < ActiveRecord::Migration[6.0]
  def change
    create_table :container_comments do |t|
      t.string :commented_by
      t.date :date
      t.text :comment
      t.references :container, foreign_key: true
      t.timestamps
    end
  end
end
