class AddForeignKeyToMerc < ActiveRecord::Migration[6.0]
  def change
    add_reference :mercs, :merc_cost_details, foreign_key: true, column: :foreign_key_column
    add_reference :mercs, :merc_customer_related_details, foreign_key: true , column: :foreign_key_column
  end
end
