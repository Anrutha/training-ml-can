class CreateLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :logs do |t|
      t.datetime :time
      t.string :activity_type
      t.string :activity_code
      t.string :status_changed_from
      t.string :status_changed_to
      t.string :done_by
      t.references :container, foreign_key: true
      t.references :activity, foreign_key: true


      t.timestamps
    end
  end
end
