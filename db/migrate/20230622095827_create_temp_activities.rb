class CreateTempActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :temp_activities do |t|
      t.references :container, null: false, foreign_key: true

      t.timestamps
    end
  end
end
