class AddRepairListTypeToCustomers < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :repair_list_type, :integer , default: 0
  end
end
