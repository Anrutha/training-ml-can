class CreateNonMaerskCustomerDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :non_maersk_customer_details do |t|
      t.string :description
      t.string :location
      t.string :lgth_qty_area
      t.string :lgth_qty_area_2
      t.string :id_source
      t.references :non_maersk_com, null: false, foreign_key: true
      t.references :non_maersk_comp, null: false, foreign_key: true
      t.references :non_maersk_dam, null: false, foreign_key: true
      t.references :non_maersk_event, null: false, foreign_key: true
      t.references :non_maersk_rep, null: false, foreign_key: true

      t.timestamps
    end
  end
end
