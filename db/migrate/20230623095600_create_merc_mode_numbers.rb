class CreateMercModeNumbers < ActiveRecord::Migration[6.0]
  def change
    create_table :merc_mode_numbers do |t|
      t.string :mode_number

      t.timestamps
    end
  end
end
