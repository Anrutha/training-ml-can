class CreateQties < ActiveRecord::Migration[6.0]
  def change
    create_table :qties do |t|
      t.string :measurement

      t.timestamps
    end
  end
end
