class CreateContainerTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :container_types do |t|

      t.timestamps
    end
  end
end
