class AddUniqueConstraintToMeasurement < ActiveRecord::Migration[6.0]
  def change
    add_index :container_lengths, :measurement, unique: true
  end
end
