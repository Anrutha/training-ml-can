class AddColumnsToContainerTypes < ActiveRecord::Migration[6.0]
  def change
    add_column :container_types, :type_name, :string
    add_index :container_types, :type_name, unique: true
  end
end
