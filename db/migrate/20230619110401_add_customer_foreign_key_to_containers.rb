class AddCustomerForeignKeyToContainers < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :containers, :customers, column: :customer_id
  end
end
