class AddColumnsToContainerHeights < ActiveRecord::Migration[6.0]
  def change
    add_column :container_heights, :measurement, :string
    add_index :container_heights, :measurement, unique: true
  end
end
