class CreateContainerRepairAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :container_repair_areas do |t|
      t.string :type_name

      t.timestamps
    end
    add_index :container_repair_areas, :type_name, unique: true
  end
end
