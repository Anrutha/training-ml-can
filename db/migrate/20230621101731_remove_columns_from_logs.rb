class RemoveColumnsFromLogs < ActiveRecord::Migration[6.0]
  def change
    remove_column :logs, :activity_type, :string
    remove_column :logs, :activity_code, :string
    remove_column :logs, :container_id, :string
  end
end
