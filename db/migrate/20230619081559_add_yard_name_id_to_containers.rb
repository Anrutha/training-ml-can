class AddYardNameIdToContainers < ActiveRecord::Migration[6.0]
  def change
    add_column :containers, :yard_name_id, :integer
    add_foreign_key :containers, :yards, column: :yard_name_id
  end
end
