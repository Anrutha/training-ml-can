class AddContainerHeightForeignKeyToContainers < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :containers, :container_heights, column: :container_height_id
  end
end
