class CreateNonMaerskComs < ActiveRecord::Migration[6.0]
  def change
    create_table :non_maersk_coms do |t|
      t.string :type_name

      t.timestamps
    end
  end
end
