class CreateContainerDamagedAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :container_damaged_areas do |t|
      t.string :type_name

      t.timestamps
    end
    add_index :container_damaged_areas, :type_name, unique: true
  end
end
