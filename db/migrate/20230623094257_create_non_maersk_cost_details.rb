class CreateNonMaerskCostDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :non_maersk_cost_details do |t|
      t.string :hours
      t.string :material_cost

      t.timestamps
    end
  end
end
