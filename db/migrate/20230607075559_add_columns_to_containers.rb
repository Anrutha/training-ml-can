class AddColumnsToContainers < ActiveRecord::Migration[6.0]
  def change
    add_column :containers, :customer_id, :integer
    add_column :containers, :container_height_id, :integer
    # add_column :containers, :container_type_id, :integer
    add_column :containers, :container_length_id, :integer
  end
end
