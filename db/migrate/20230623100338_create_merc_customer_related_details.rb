class CreateMercCustomerRelatedDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :merc_customer_related_details do |t|
      t.references :merc_repair_mode, null: false, foreign_key: true
      t.string :combined
      t.string :id_source
      t.string :description
      t.references :merc_mode_number, null: false, foreign_key: true
      t.string :repair_code

      t.timestamps
    end
  end
end
