class AddUniqueConstraintToContainerNum < ActiveRecord::Migration[6.0]
  def change
    add_index :containers, :container_num, unique: true
  end
end
