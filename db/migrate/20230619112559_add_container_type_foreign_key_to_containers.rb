class AddContainerTypeForeignKeyToContainers < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :containers, :container_types, column: :container_type_id

  end
end
