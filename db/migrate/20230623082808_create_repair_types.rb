class CreateRepairTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :repair_types do |t|
      t.string :type_name

      t.timestamps
    end
    add_index :repair_types, :type_name, unique: true
  end
end
