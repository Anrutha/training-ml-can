class CreateMercRepairModes < ActiveRecord::Migration[6.0]
  def change
    create_table :merc_repair_modes do |t|
      t.string :mode_number

      t.timestamps
    end
  end
end
