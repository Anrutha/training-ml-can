class AddForeignKeyToNonMaersk < ActiveRecord::Migration[6.0]
  def change
    add_reference :non_maersks, :non_maersk_cost_details, foreign_key: true, column: :foreign_key_column
    add_reference :non_maersks, :non_maersk_customer_details, foreign_key: true , column: :foreign_key_column
  end
end
