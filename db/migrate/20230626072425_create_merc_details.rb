class CreateMercDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :merc_details do |t|
      t.references :merc_customer_related_detail, null: false, foreign_key: true
      t.references :merc_cost_detail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
