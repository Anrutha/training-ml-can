class RenameQtyColumn < ActiveRecord::Migration[6.0]
  def up
    rename_column :activity_details, :qty, :qty_id
    change_column :activity_details, :qty_id, 'integer USING qty_id::integer'
  end

  def down
    change_column :activity_details, :qty_id, :string
    rename_column :activity_details, :qty_id, :qty
  end
end
