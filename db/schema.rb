# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_06_30_072858) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "activity_type"
    t.string "activity_code"
    t.string "status"
    t.date "date"
    t.string "current_user"
    t.bigint "container_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["container_id"], name: "index_activities_on_container_id"
  end

  create_table "activity_details", force: :cascade do |t|
    t.bigint "activity_id", null: false
    t.bigint "repair_list_detail_id", null: false
    t.integer "qty_id"
    t.string "location"
    t.string "comment"
    t.integer "damaged_area_phot_id"
    t.integer "repaired_area_photo_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["activity_id"], name: "index_activity_details_on_activity_id"
    t.index ["repair_list_detail_id"], name: "index_activity_details_on_repair_list_detail_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_cities_on_name", unique: true
  end

  create_table "comments", force: :cascade do |t|
  end

  create_table "container_comments", force: :cascade do |t|
    t.string "commented_by"
    t.date "date"
    t.text "comment"
    t.bigint "container_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["container_id"], name: "index_container_comments_on_container_id"
  end

  create_table "container_damaged_areas", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["type_name"], name: "index_container_damaged_areas_on_type_name", unique: true
  end

  create_table "container_heights", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "measurement"
    t.index ["measurement"], name: "index_container_heights_on_measurement", unique: true
  end

  create_table "container_lengths", force: :cascade do |t|
    t.string "measurement"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["measurement"], name: "index_container_lengths_on_measurement", unique: true
  end

  create_table "container_repair_areas", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["type_name"], name: "index_container_repair_areas_on_type_name", unique: true
  end

  create_table "container_types", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "type_name"
    t.index ["type_name"], name: "index_container_types_on_type_name", unique: true
  end

  create_table "containers", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "container_num"
    t.string "container_owner_name"
    t.string "submitter_initials"
    t.integer "container_manufacture_year"
    t.string "location"
    t.text "comments"
    t.integer "door_photo_including_container_number_id"
    t.integer "left_side_photo_id"
    t.integer "right_side_photo_id"
    t.integer "front_side_photo_id"
    t.integer "interior_photo_id"
    t.integer "underside_photo_id"
    t.integer "roof_photo_id"
    t.integer "csc_plate_number_id"
    t.integer "customer_id"
    t.integer "container_height_id"
    t.integer "container_type_id"
    t.integer "container_length_id"
    t.integer "yard_name_id"
    t.index ["container_num"], name: "index_containers_on_container_num", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "customer_name"
    t.string "email"
    t.string "owner_name"
    t.string "billing_name"
    t.string "hourly_rate"
    t.string "gst"
    t.string "pst"
    t.integer "city_id"
    t.string "address"
    t.string "province"
    t.string "postal_code"
    t.string "password"
    t.integer "status", default: 0
    t.integer "repair_list_type", default: 0
  end

  create_table "items", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "logs", force: :cascade do |t|
    t.datetime "time"
    t.string "status_changed_from"
    t.string "status_changed_to"
    t.string "done_by"
    t.bigint "activity_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "container_id", null: false
    t.index ["activity_id"], name: "index_logs_on_activity_id"
    t.index ["container_id"], name: "index_logs_on_container_id"
  end

  create_table "merc_cost_details", force: :cascade do |t|
    t.string "max_mat_cost"
    t.string "unit_mat_cost"
    t.string "hours_per_unit"
    t.string "max_pieces"
    t.string "units"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "merc_customer_related_details", force: :cascade do |t|
    t.bigint "merc_repair_mode_id", null: false
    t.string "combined"
    t.string "id_source"
    t.string "description"
    t.bigint "merc_mode_number_id", null: false
    t.string "repair_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["merc_mode_number_id"], name: "index_merc_customer_related_details_on_merc_mode_number_id"
    t.index ["merc_repair_mode_id"], name: "index_merc_customer_related_details_on_merc_repair_mode_id"
  end

  create_table "merc_details", force: :cascade do |t|
    t.bigint "merc_customer_related_detail_id", null: false
    t.bigint "merc_cost_detail_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["merc_cost_detail_id"], name: "index_merc_details_on_merc_cost_detail_id"
    t.index ["merc_customer_related_detail_id"], name: "index_merc_details_on_merc_customer_related_detail_id"
  end

  create_table "merc_mode_numbers", force: :cascade do |t|
    t.string "mode_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "merc_repair_modes", force: :cascade do |t|
    t.string "mode_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mercs", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "merc_cost_details_id"
    t.bigint "merc_customer_related_details_id"
    t.index ["merc_cost_details_id"], name: "index_mercs_on_merc_cost_details_id"
    t.index ["merc_customer_related_details_id"], name: "index_mercs_on_merc_customer_related_details_id"
  end

  create_table "non_maersk_comps", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersk_coms", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersk_cost_details", force: :cascade do |t|
    t.string "hours"
    t.string "material_cost"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersk_customer_details", force: :cascade do |t|
    t.string "description"
    t.string "location"
    t.string "lgth_qty_area"
    t.string "lgth_qty_area_2"
    t.string "id_source"
    t.bigint "non_maersk_com_id", null: false
    t.bigint "non_maersk_comp_id", null: false
    t.bigint "non_maersk_dam_id", null: false
    t.bigint "non_maersk_event_id", null: false
    t.bigint "non_maersk_rep_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["non_maersk_com_id"], name: "index_non_maersk_customer_details_on_non_maersk_com_id"
    t.index ["non_maersk_comp_id"], name: "index_non_maersk_customer_details_on_non_maersk_comp_id"
    t.index ["non_maersk_dam_id"], name: "index_non_maersk_customer_details_on_non_maersk_dam_id"
    t.index ["non_maersk_event_id"], name: "index_non_maersk_customer_details_on_non_maersk_event_id"
    t.index ["non_maersk_rep_id"], name: "index_non_maersk_customer_details_on_non_maersk_rep_id"
  end

  create_table "non_maersk_dams", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersk_events", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersk_reps", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "non_maersks", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "non_maersk_cost_details_id"
    t.bigint "non_maersk_customer_details_id"
    t.index ["non_maersk_cost_details_id"], name: "index_non_maersks_on_non_maersk_cost_details_id"
    t.index ["non_maersk_customer_details_id"], name: "index_non_maersks_on_non_maersk_customer_details_id"
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer "resource_owner_id"
    t.integer "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "qties", force: :cascade do |t|
    t.string "measurement"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "repair_list_details", force: :cascade do |t|
    t.bigint "repair_type_id", null: false
    t.bigint "merc_id", null: false
    t.bigint "non_maersk_id", null: false
    t.bigint "container_repair_area_id", null: false
    t.bigint "container_damaged_area_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["container_damaged_area_id"], name: "index_repair_list_details_on_container_damaged_area_id"
    t.index ["container_repair_area_id"], name: "index_repair_list_details_on_container_repair_area_id"
    t.index ["merc_id"], name: "index_repair_list_details_on_merc_id"
    t.index ["non_maersk_id"], name: "index_repair_list_details_on_non_maersk_id"
    t.index ["repair_type_id"], name: "index_repair_list_details_on_repair_type_id"
  end

  create_table "repair_types", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["type_name"], name: "index_repair_types_on_type_name", unique: true
  end

  create_table "status_types", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "temp_activities", force: :cascade do |t|
    t.bigint "container_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["container_id"], name: "index_temp_activities_on_container_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "yards", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "yard_name"
    t.index ["yard_name"], name: "index_yards_on_yard_name", unique: true
  end

  add_foreign_key "activities", "containers"
  add_foreign_key "activity_details", "activities"
  add_foreign_key "activity_details", "repair_list_details"
  add_foreign_key "container_comments", "containers"
  add_foreign_key "containers", "container_heights"
  add_foreign_key "containers", "container_lengths"
  add_foreign_key "containers", "container_types"
  add_foreign_key "containers", "customers"
  add_foreign_key "containers", "yards", column: "yard_name_id"
  add_foreign_key "logs", "activities"
  add_foreign_key "logs", "containers"
  add_foreign_key "merc_customer_related_details", "merc_mode_numbers"
  add_foreign_key "merc_customer_related_details", "merc_repair_modes"
  add_foreign_key "merc_details", "merc_cost_details"
  add_foreign_key "merc_details", "merc_customer_related_details"
  add_foreign_key "mercs", "merc_cost_details", column: "merc_cost_details_id"
  add_foreign_key "mercs", "merc_customer_related_details", column: "merc_customer_related_details_id"
  add_foreign_key "non_maersk_customer_details", "non_maersk_comps"
  add_foreign_key "non_maersk_customer_details", "non_maersk_coms"
  add_foreign_key "non_maersk_customer_details", "non_maersk_dams"
  add_foreign_key "non_maersk_customer_details", "non_maersk_events"
  add_foreign_key "non_maersk_customer_details", "non_maersk_reps"
  add_foreign_key "non_maersks", "non_maersk_cost_details", column: "non_maersk_cost_details_id"
  add_foreign_key "non_maersks", "non_maersk_customer_details", column: "non_maersk_customer_details_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "repair_list_details", "container_damaged_areas"
  add_foreign_key "repair_list_details", "container_repair_areas"
  add_foreign_key "repair_list_details", "mercs"
  add_foreign_key "repair_list_details", "non_maersks"
  add_foreign_key "repair_list_details", "repair_types"
  add_foreign_key "temp_activities", "containers"
end
